#!/usr/local/bin/lua
---------------------------------------------------------------------
--     This Lua5 script is Copyright (c) 2019, Peter J Billam      --
--                       www.pjb.com.au                            --
--  This script is free software; you can redistribute it and/or   --
--         modify it under the same terms as Lua5 itself.          --
---------------------------------------------------------------------
local Version = '1.0  for Lua5'
local VersionDate  = '25jun2019'
local Synopsis = [[
program_name [options] [filenames]
]]
local iarg=1; while arg[iarg] ~= nil do
	if not string.find(arg[iarg], '^-[a-z]') then break end
	local first_letter = string.sub(arg[iarg],2,2)
	if first_letter == 'v' then
		local n = string.gsub(arg[0],"^.*/","",1)
		print(n.." version "..Version.."  "..VersionDate)
		os.exit(0)
	elseif first_letter == 'c' then
		whatever()
	else
		local n = string.gsub(arg[0],"^.*/","",1)
		print(n.." version "..Version.."  "..VersionDate.."\n\n"..Synopsis)
		os.exit(0)
	end
	iarg = iarg+1
end 

function new_tribonacci_number ()
	local numbers = { 1, 4, 7, 13 }
	local i = 0
	return function ()
		i = i + 1
		if i <= #numbers then
			return numbers[i]
		else
			numbers[i] = numbers[i-1] + numbers[i-2] + numbers[i-3]
			return numbers[i]
		end
	end
end

function new_tribonacci_1 ()
	-- https://en.wikipedia.org/wiki/Rauzy_fractal
	-- better to use: for n>2  t[n] = t[n−1]..t[n−2]..t[n−3]
	-- or could try arrays of chars instead of strings...
	-- benchmark these !!
	local words = { '1', '12', '1213', '1213121', '1213121121312' }
	local word = '1213121121312'
	local i = 0
	local subs = { ['1']='12', ['2']='13', ['3']='1' }
	return function ()
		i = i + 1
		if i <= string.len(word) then
			return string.sub(word, i, i)
		else
			word = string.gsub(word, '1', 'XY')
			word = string.gsub(word, '2', 'XZ')
			word = string.gsub(word, '3', '1')
			word = string.gsub(word, 'X', '1')
			word = string.gsub(word, 'Y', '2')
			word = string.gsub(word, 'Z', '3')
			return string.sub(word, i, i)
		end
	end
end

function new_tribonacci_3 ()
	-- https://en.wikipedia.org/wiki/Rauzy_fractal
	-- use: for n > 2 t[n] = t[n−1]..t[n−2]..t[n−3]
	-- or could try arrays of chars instead of strings...
	-- benchmark these !!
	local words = {
		{ 1 },
		{ 1, 2 },
		{ 1, 2, 1, 3 },
		{ 1, 2, 1, 3, 1, 2, 1 },
		{ 1, 2, 1, 3, 1, 2, 1, 1, 2, 1, 3, 1, 2 },
	}
	local i = 0
	local n = 4
	return function ()
		i = i + 1
		if i <= #words[n] then
			return words[n][i]
		else
			n = n + 1
			words[n] = { }
			for j = 1, #words[n-1] do
				words[n][j] = words[n-1][j]
			end
			local index = #words[n]
			for j = 1, #words[n-2] do
				index = index + 1
				words[n][index] = words[n-2][j]
			end
			for j = 1, #words[n-3] do
				index = index + 1
				words[n][index] = words[n-3][j]
			end
			return words[n][i]
		end
	end
end

function new_tribonacci ()
	-- https://en.wikipedia.org/wiki/Rauzy_fractal
	-- use: for n > 2 t[n] = t[n−1]..t[n−2]..t[n−3]
	-- or could try arrays instead of strings...
	-- benchmark shows strcat is clearly faster :-)
	local words = { '1', '12', '1213', '1213121', '1213121121312' }
	local i = 0
	local n = 4
	return function ()
		i = i + 1
		if i <= string.len(words[n]) then
			return string.sub(words[n], i, i)
		else
			n = n + 1
			words[n] = words[n-1]..words[n-2]..words[n-3]
			return string.sub(words[n], i, i)
		end
	end
end

local tribonacci_number = new_tribonacci_number()
for i = 1,17 do
	io.stdout:write(tostring(tribonacci_number()))
	io.stdout:write(' ')
	end
io.stdout:write('\n')

local x0 = os.clock()
local tribonacci = new_tribonacci_1()
--for i = 1,72 do io.stdout:write(tribonacci()) end io.stdout:write('\n')
for i = 1,1000000 do local c = tribonacci() end

local x1 = os.clock()
print('1', x1-x0)

local tribonacci = new_tribonacci()
--for i = 1,72 do io.stdout:write(tribonacci()) end io.stdout:write('\n')
for i = 1,1000000 do local c = tribonacci() end

local x2 = os.clock()
print('2', x2-x1)

local tribonacci = new_tribonacci_3()
--for i = 1,72 do io.stdout:write(tribonacci()) end io.stdout:write('\n')
for i = 1,1000000 do local c = tribonacci() end

local x3 = os.clock()
print('3', x3-x2)

-- 1 4 7 13 24 44 81 149 274 504 927 1705 3136 5768 10609 19513 35890 
-- 1   0.691816
-- 2   0.296349   <== so new_tribonacci is clearly fastest (in lua)
-- 3   0.587223


--[=[

=pod

=head1 NAME

program_name - what it does

=head1 SYNOPSIS

 program_name infile > outfile

=head1 DESCRIPTION

This script

=head1 ARGUMENTS

=over 3

=item I<-v>

Print the Version

=back

=head1 DOWNLOAD

This at is available at

=head1 AUTHOR

Peter J Billam, http://pjb.com.au/comp/contact.html

=head1 SEE ALSO

 http://pjb.com.au/

=cut

]=]
