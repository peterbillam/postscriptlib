#!/usr/bin/env lua
---------------------------------------------------------------------
--     This Lua5 script is Copyright (c) 2022, Peter J Billam      --
--                         pjb.com.au                              --
--  This script is free software; you can redistribute it and/or   --
--         modify it under the same terms as Lua5 itself.          --
---------------------------------------------------------------------

ST = require 'spanning_tree'
RA = require 'randomdist'

-- spanning_tree needs lua, not perl !

-- Movies on film are almost exclusively projected at 24 fps.
-- Television, however, does not have an internationally accepted frame rate.
-- In Europe and many other countries, PAL and SECAM use 25 fps,
-- whereas NTSC video in North America and Japan uses 29.97 fps.
-- Other common frame rates are usually multiples of these.
--   avconv -i spanning_tree_mus.wav \
--     -f image2 -r 24 -i t_%06d.jpg \
--     -s 1600x900 -c:a mp3 t.mp4

-- https://stackoverflow.com/questions/23236898/add-text-on-image-at-specific-point-using-imagemagick/23238369
-- convert -pointsize 40 -fill blue -draw 'text 600,600 "Love You Mom"' temp1.jpg temp2.jpg
-- https://legacy.imagemagick.org/Usage/anim_mods/#compose_draw
---------------------------------------------------------------
function which(s)
	local P = require 'posix'
	local f
	for i,d in ipairs(split(os.getenv('PATH'), ':')) do
		f=d..'/'..s; if P.access(f, 'x') then return f end
	end
end
function split(s, pattern, maxNb) -- http://lua-users.org/wiki/SplitJoin
	if not s or string.len(s)<2 then return {s} end
	if not pattern then return {s} end
	if maxNb and maxNb <2 then return {s} end
	local result = { }
	local theStart = 1
	local theSplitStart,theSplitEnd = string.find(s,pattern,theStart)
	local nb = 1
	while theSplitStart do
		table.insert( result, string.sub(s,theStart,theSplitStart-1) )
		theStart = theSplitEnd + 1
		theSplitStart,theSplitEnd = string.find(s,pattern,theStart)
		nb = nb + 1
		if maxNb and nb >= maxNb then break end
	end
	table.insert( result, string.sub(s,theStart,-1) )
	return result
end
---------------------------------------------------------------

local Version = '1.0  for Lua5'
local VersionDate  = '7feb2022'
local Synopsis = [[
  make_spanning_tree_video [options] [filenames]
]]
TmpDir       = "/tmp/spanning_tree_video"
FrameNumber  = 0      -- used and incremented by next_filename()
Width        = 1600   -- or 1760
Height       = 900    -- or  990
Fps          = 16.0   -- or   25 or 29.97
NFrames      = 700    
PrePad       = 5      -- seconds
PostPad      = 5      -- seconds
Mencoder     = which('mencoder')
Avconv       = which('avconv')
Ffmpeg       = which('ffmpeg')
Points = {}

----------------------------------------------------
function warn(...)
	local a = {}
	for k,v in pairs{...} do table.insert(a, tostring(v)) end
	io.stderr:write(table.concat(a),'\n') ; io.stderr:flush()
end
function die(...) warn(...);  os.exit(1) end

function printf (...) print(string.format(...)) end
local function round(x)
	if not x then return nil end
	return math.floor(x+0.5)
end
----------------------------------------------------
function next_filename ()  -- should really be a closure...
	FrameNumber = FrameNumber + 1
	return string.format("t_%06d.jpg", FrameNumber)
end
function this_filename ()
	return string.format("t_%06d.jpg", FrameNumber)
end

function eps2jpg (eps_text, fn)
	warn(string.format("fn=%s",fn))
	local tmpfile = '/tmp/spanning_tree_tmpfile'
	local T = io.open(tmpfile, 'w')
	T:write(eps_text)
	T:close()
	-- print(string.format(
	os.execute(string.format(
	  -- "gs -sDEVICE=jpeg -sOutputFile=%s -q -g%sx%s -r72 %s",
	  --  fn, Width, Height, tmpfile ))
	  "gs -sDEVICE=jpeg -sOutputFile=%s -q -g%sx%s -r72 - < %s",
	    fn, Width, Height, tmpfile ))
	-- https://luaposix.github.io/luaposix/modules/posix.sys.wait.html
	-- might have to run ps, extract the PID of gs, and wait for that ?
	-- OR: put eps_text in a tmpfile then run gs with os.execute('gs ...') ?
end

function title2jpg ()
	local jpg_filename = 'pre.jpg'
	eps2jpg( [[%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: 0 0 ]]..Width..' '..Height..[[

% http://www.pjb.com.au
%%BeginProlog
(/home/pjb/ps/lib/colours.ps) run
(/home/pjb/ps/lib/fonts.ps) run
/xmax ]]..Width..[[ def
/ymax ]]..Height..[[ def
/xmid xmax 0.5 mul def
/ymid ymax 0.5 mul def
/centreshow { % usage: x y font fontsize (string) centreshow
  3 dict begin
    [ /s /fontsize /font ] { exch def } forall
    gsave moveto font findfont fontsize scalefont setfont
    gsave s false charpath flattenpath pathbbox grestore
    exch 4 -1 roll pop pop s stringwidth pop -0.5 mul  % dx/2
    3 1 roll sub 0.5 mul % dy/2
    rmoveto s show grestore
  end
} bind def
%%EndProlog
%%Page: 1 1
%%BeginPageSetup
%%EndPageSetup
white setrgbcolor  0 0 xmax ymax rectfill
.4 .4 .4 setrgbcolor
xmid 0.96 mul ymax .62 mul /FatSimpleItalic xmax 12 div
  (Prim's Algorithm) centreshow
xmid ymax .35 mul /FatSimpleItalic xmax 32 div
  (in slow motion) centreshow
xmid ymax .20 mul /FatSimpleItalic xmax 37 div
  (Peter Billam,  2022) centreshow
showpage
%%EOF
]], next_filename())
	-- close P; ??  -- wait;
end

function credits2jpg (iframe)
	local jpg_filename = next_filename()
	warn(string.format("\027[K jpg_filename = %s\027[A", jpg_fn))
	local glide = 1.0;   if iframe < 72 then glide = iframe/72 end
	local fade  = 0.0;   if iframe >120 then fade  = (iframe-120)/120 end
	if fade > 1.0 then fade = 1.0 end
	glide = string.format('%g', glide)
	fade  = string.format('%g', fade)
	eps2jpg([[%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: 0 0 ]]..Width..' '..Height..[[
% http://www.pjb.com.au
%%BeginProlog
(/home/pjb/ps/lib/colours.ps) run
(/home/pjb/ps/lib/fonts.ps) run
/xmax ]]..Width..[[ def
/ymax ]]..Height..[[ def
/xmid xmax 0.5 mul def
/ymid ymax 0.5 mul def
/centreshow { % usage: x y font fontsize (string) centreshow
  3 dict begin
    [ /s /fontsize /font ] { exch def } forall
    gsave moveto font findfont fontsize scalefont setfont
    gsave s false charpath flattenpath pathbbox grestore
    exch 4 -1 roll pop pop s stringwidth pop -0.5 mul  % dx/2
    3 1 roll sub 0.5 mul % dy/2
    rmoveto s show grestore
  end
} bind def
/lininterp {   % x1 x2 lininterp  ==>  x
	]]..glide..[[ mul exch    1.0 ]]..glide..[[ sub mul add
} def
%%EndProlog
%%Page: 1 1
%%BeginPageSetup
%%EndPageSetup
blue setrgbcolor  0 0 xmax ymax rectfill
white 1.0 ]]..fade..[[ sub blue ]]..fade..[[ rgbmix setrgbcolor
xmid ymax 0.5 .65 lininterp mul
  /Helvetica-Bold   0.01 xmax 25 div lininterp
  (www.pjb.com.au/comp/free/) centreshow
xmax 0.3 mul xmid lininterp ymax .50 mul
  /Helvetica-Bold   0.01 xmax 25 div lininterp
  (www.pjb.com.au/comp) centreshow
xmax 0.7 mul xmid lininterp ymid ymax .35 mul lininterp
  /Helvetica-Bold   0.01 xmax 25 div lininterp
  (www.pjb.com.au) centreshow
showpage
%%EOF
EOT
]], jpg_filename);
	-- close P; wait;
end

function make_points (npoints)
	local points = {}
	for i = 1,npoints do
		points[i] = {1550*math.random(), 850*math.random() }
	end
	return points
end
function distance_func (point1, point2)
    local dx = point2[1] - point1[1]
    local dy = point2[2] - point1[2]
    return math.sqrt(dx*dx + dy*dy)
end

function prim_in_slomo(points, distance_func)
	local jpg_fn = next_filename()
	warn(string.format("\027[K jpg_filename = %s\027[A", jpg_fn))
	local src = ST.gnuplot_src(points,{},{}, Width, Height, jpg_fn)
	ST.gnuplot_run(src)
	freeze(2*Fps)
	local links, distances = ST.prim(points, distance_func)
	for iframe = 1, #points-1 do
		local jpg_fn = next_filename()
		warn(string.format("\027[K jpg_filename = %s\027[A", jpg_fn))
		local lnks = {}
		for i = 1, iframe do lnks[i] = links[i] end
		local src = ST.gnuplot_src(
		  points,lnks,distances, Width,Height, jpg_fn)
		ST.gnuplot_run(src)
	end
	return
end

Points = make_points(200)
Grand  = RA.new_grand(0,1)
function frame2jpg (iframe)
-- warn(string.format('#Points=%d',#Points))
-- warn(string.format('Points[13]=%s',table.concat(Points[13],',')))
	local jpg_fn = next_filename()
	warn(string.format("\027[K jpg_filename = %s\027[A", jpg_fn))
	for i = 1, #Points do
		local new_x = Points[i][1] + Grand()
		local new_y = Points[i][2] + Grand()
		Points[i][1] = new_x ; Points[i][2] = new_y
	end
-- warn('#distances=%d  distances=%s',#distances,table.concat(distances,' '))
	local src = ST.gnuplot_src(Points,links,distances, Width,Height, jpg_fn)
	ST.gnuplot_run(src)
end

----------------------------------------------------

local iarg=1; while arg[iarg] ~= nil do
	if not string.find(arg[iarg], '^-[a-z]') then break end
	local first_letter = string.sub(arg[iarg],2,2)
	if first_letter == 'v' then
		local n = string.gsub(arg[0],"^.*/","",1)
		print(n.." version "..Version.."  "..VersionDate)
		os.exit(0)
	elseif first_letter == 'c' then
		whatever()
	else
		local n = string.gsub(arg[0],"^.*/","",1)
		print(n.." version "..Version.."  "..VersionDate.."\n\n"..Synopsis)
		os.exit(0)
	end
	iarg = iarg+1
end
-- if arg[iarg] then F = assert(io.open(arg[iarg], 'r')) else F = io.stdin end
-- c = {utf8.codepoint(F:read('a'),1,-1)}

if not Mencoder and not Ffmpeg and not Avconv then
	die(" Sorry, can't find avconv, ffmpeg nor mencoder in your $PATH\n")
end

function mkdir(dir)
	local sys_stat = require "posix.sys.stat"
	sys_stat.mkdir(dir) -- [, mode=511])
	return true
end
function chdir(dir)
	local unistd = require "posix.unistd"
	local status, errstr, errno = unistd.chdir(dir)
	if status == 0 then return true else return nil, errstr end
end
function link(oldpath, newpath)
	local unistd = require "posix.unistd"
	return 0 == unistd.link(oldpath, newpath)
end

function freeze (nframes)
	nframes = round(nframes)
	local this = this_filename()
	for i = 1,nframes do
		link(this, next_filename())
	end
end
function cross_fade (nframes, start_fn, finish_fn)
	-- https://legacy.imagemagick.org/Usage/compose/#blend
	-- composite -blend {percent} overlay bgnd result
	for iframe = 1,nframes do
		local percent = tostring(100 - round(100*iframe/nframes))
		os.execute(string.format('composite -blend %s %s %s %s',
		  percent, start_fn, finish_fn, next_filename()))
	end
end
-- https://legacy.imagemagick.org/script/color.php
-- eg: '#00ff00'
function fade_to_white (nframes)
	os.execute(string.format(
	  'convert -size %dx%d gradient:white-white white.png',
	   Width, Height))
	cross_fade (nframes, this_filename(), 'white.png')
end
function fade_to_black (nframes)
	os.execute(string.format(
	  'convert -size %dx%d gradient:black-black black.png',
	   Width, Height))
	cross_fade (nframes, this_filename(), 'black.png')
end

if not mkdir(TmpDir) then die(printf("can't mkdir %s",TmpDir)) end
if not chdir(TmpDir) then die(printf("can't chdir %s",TmpDir)) end
warn(printf(" TmpDir = %s and pwd gives:",TmpDir)) ; os.execute('pwd')

title2jpg()
freeze(3*Fps)
local start_fn = this_filename()
fade_to_white(3*Fps, start_fn)

warn("\nMovie:")
-- for iframe = 1, NFrames do frame2jpg(iframe) end
-- local links, distances = ST.prim(Points, distance_func)
prim_in_slomo(Points, distance_func)
start_fn = this_filename()
fade_to_white(4*Fps, start_fn)
freeze(Fps)

-- warn("\nCredits:")
-- for iframe = 1, 250 do credits2jpg(iframe) end
print('finished movie, pwd gives:')
os.execute('pwd')

if Avconv then
	-- for %06 see man ffmpeg "For creating a video from many images"
	os.execute(string.format(
	  -- "%s -i spanning_tree_mus.wav -f image2 -r %s -i t_%%06d.jpg "
	  "%s -f image2 -r %s -i t_%%06d.jpg "
	  .. " -s %sx%s t.mp4", Avconv, tostring(Fps), Width, Height))
elseif Ffmpeg then
	-- "image2" is the image file demuxer,
	--   which reads from a list of image files specified by a pattern.
	  -- "%s -i spanning_tree_mus.wav -f image2 -r %s -i t_%%06d.jpg "
	os.execute(string.format(
	  "%s -f image2 -r %s -i t_%%06d.jpg -s %sx%s t.mp4",
	   Ffmpeg, tostring(Fps), Width, Height))
elseif Mencoder then
	-- mp3lame, faac and x264enc are not necessarily compiled in; pcm works.
	local aspect = string.format('%5g', Width/Height)
	os.execute(string.format(
	"%s -audiofile spanning_tree_mus.wav -oac pcm -fps %s -ofps %s "
	  .. "'mf://t_*.jpg' -mf h=$Height:w=$Width:fps=$Fps:type=jpeg "
	  ..  "-o t.mp4 -ovc lavc -lavcopts vcodec=mpeg4:aspect=%s",
	  Mencoder, tostring(Fps), tostring(Fps),
	  Height, Width, tostring(Fps), aspect))
else
	die( " BUG: shouldn't reach here...")
end

os.execute("mplayer t.mp4")
-- spanning_tree_mus.wav: No such file or directory

warn(string.format("\n your video is: %s/t.mp4", TmpDir))

-- clean up ?

os.exit()




--[[

=pod

=head1 NAME

make_connett_video - synthesises a video of John Connett's pattern

=head1 SYNOPSIS

 make_connett_video

=head1 DESCRIPTION

This script uses I<gs> to generate a jpg for each frame,
and then either I<avconv> or I<ffmpeg> or I<mencoder>
to put them together into an mp4 video.

In the following, the screen is covered with tiny squares,
size I<dx> times I<dx>.
aligned vertically and horizontally in rows and columns,
each square filled with one colour.
It is located at I<x,y>, with respect to an I<origin> at 0,0.
These coordinates I<x,y> and the I<origin> and I<dx> are in real screen-pixels, 
The colour is then chosen as follows:

  r^2 = x^2 + y^2               r = radial distance from the origin
  c = integer part of (a r^2)   where a is some constant
  color = c modulo (number of colors)

At first glance, the algorithm looks circularly symmetric,
and so should just paint concentric coloured rings.
Indeed when I<a> is small, that looks to be true - 
but as I<c> becomes greater than the number of colours,
then the fact that the squares are aligned in rows and columns
becomes important.

If I<y = 0> then, when I<c> is a multiple of I<ncolours>
the colour is the same as at the origin; 
indeed concentric coloured rings develop, just like round the origin, since

  if   (a x^2) modulo ncolours  =  0
  then (a x^2 + a y^2) modulo ncolours = a y^2 modulo ncolours

(In other words, r^2 has a Manhattan Metric,)
so a regular grid of concentric-rings appears

  a = ncolours / ( 2 * dx * grid )

(although I don't understand why the factor 2 is necessary ...)

As the grid size shrinks, larger-sacle metapatterns appear and
become more dominant.
The metapattern fills the screen when grid is an integer multiple of dx;
this is what happens as I<grid> approaches I<dx> at the end of the video.

=head1 OPTIONS

=over 3

=item I<-v>

Prints version number.

=back

=head1 CHANGES

 20170526  1.0  first working version

=head1 AUTHOR

Peter J Billam   http://www.pjb.com.au/comp/contact.html

=head1 CREDITS

Based on the algorithm by John E. Connett, of the University of Minnesota,
originally published by A. K. Dewdney in his "Computer Recreations" column
in the September 1986 issue of Scientific American.

=head1 SEE ALSO

 http://www.nahee.com/spanky/www/fractint/fractal_types.html
 http://www.nahee.com/spanky/www/fractint/circle_type.html
 https://www.jstor.org/stable/2686389?
 http://www.mpeters.de/mpeweb/hop/files/interfac.txt
 http://xochipilli.com/content/circlesquared/
 Dewdney's 'Computer Recreations' in Scientific American, Sept 1986
 https://en.wikipedia.org/wiki/Taxicab_geometry
 http://www.pjb.com.au/
 perl(1).

=cut

--]]
